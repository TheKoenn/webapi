package team.bits.webapi

import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.http.HttpMethod
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.util.KtorExperimentalAPI
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin

@KtorExperimentalAPI
class WebAPI : JavaPlugin() {

    companion object {
        val versionRegex = Regex("\\(MC: ([0-9.]+)\\)")
    }

    private val server = embeddedServer(Netty, 8080) {
        install(ContentNegotiation) {
            jackson {
                enable(SerializationFeature.INDENT_OUTPUT)
            }
        }

        install(CORS) {
            method(HttpMethod.Options)
            method(HttpMethod.Get)
            anyHost()
        }

        routing {
            get("/") {
                val bukkit = Bukkit.getServer()
                call.respond(mapOf(
                        "online" to true,
                        "motd" to bukkit.motd,
                        "players" to mapOf(
                                "online" to bukkit.onlinePlayers.size,
                                "max" to bukkit.maxPlayers,
                                "list" to bukkit.onlinePlayers.stream().map { player ->
                                    mapOf(
                                            "name" to player.name,
                                            "uuid" to player.uniqueId
                                    )
                                }.toArray()
                        ),
                        "version" to (versionRegex.find(bukkit.version)?.groupValues?.get(1) ?: "unknown")
                ))
            }
        }
    }

    override fun onEnable() {
        server.start(false)
    }
}